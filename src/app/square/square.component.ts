import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-square',
  templateUrl: './square.component.html',
  styleUrls: ['./square.component.css']
})
export class SquareComponent {
  @Input() hasItem = false;
  @Output() foundBomb = new EventEmitter<boolean>();
  isClicked = false;

  onClickSquare() {
    this.isClicked = true;
    if(this.hasItem && this.isClicked) this.foundBomb.emit(this.isClicked);
  }

  getClassNames() {
    return {
      closedSquare: !this.isClicked,
      openedSquare: this.isClicked
    }
  }
}
