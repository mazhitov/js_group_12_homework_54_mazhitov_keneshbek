import {Component} from '@angular/core';

type squareType = {
  hasItem: boolean;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  squareArray: squareType[] = [];
  numberOfTries = 0;
  isFoundBomb = false;
  constructor() {
    this.generateArray();
    this.chooseSubject();
  }

  generateArray() {
    for (let i = 0; i < 36; i++) {
      this.squareArray.push({hasItem: false});
    }
  }

  chooseSubject() {
    const index = Math.floor(Math.random() * 36);
    this.squareArray[index].hasItem = true;
  }

  onClickTryGuess() {
    this.numberOfTries++;
  }

  onClickReset() {
    this.squareArray = [];
    this.numberOfTries = 0;
    this.isFoundBomb = false;
    this.generateArray();
    this.chooseSubject();
  }

  onFoundBomb() {
    this.isFoundBomb = true;
  }
  getClassName() {
    return {
      container: true,
      containerDisabled: this.isFoundBomb
    }
  }
}
